function solution (N , users) {
    let answer = []


    for(let i = 1; i<=N; i++){   
        const notYetCleared = users.filter(y=> y === i).length
        const hasCleared = users.filter(y=> y >= i ).length

        answer.push({
            stage:i,
            failure_rate: notYetCleared / hasCleared
        })
    }
    answer.sort((a , b)=>{
        if(a.failure_rate === b.failure_rate) {
            return a.stage - b.stage
        } else {
            return b.failure_rate - a.failure_rate
        }
    })
    answer = answer.map(x=> x.stage)
    return answer
}

console.log(solution(5 , [2,1,2,6,2,4,3,3]));
console.log(solution(4 , [4,4,4,4,4]));


