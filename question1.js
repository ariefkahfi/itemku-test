function updateMessage(answerArr , userid , displayName) {
    answerArr
        .filter(x=> x.userid == userid)
        .map(x=> {
            const {message} = x
            let splitMessage = message.split(" ")
            splitMessage[0] = displayName
            x.message = splitMessage.join(" ")
            return x
        })
}


function solution(record) {
    let answer = []
    
    record.forEach(x=>{
        const splitRecord = x.split(" ")
        const cmd = splitRecord[0]
        const userid = splitRecord[1]
        const displayName = splitRecord[2]

        if(answer.length <= 0) {
            answer.push({
                userid,
                message: `${displayName} came in`
            })
        } else {
            if(cmd == "Enter") {
                answer.push({
                    userid,
                    message:`${displayName} came in`
                })
                updateMessage(answer , userid, displayName)
            } else if (cmd == "Change") {
                updateMessage(answer , userid , displayName)
            } else if (cmd == "Leave") {
                const obj = answer.find(x=> x.userid == userid)
                
                answer.push({
                    userid,
                    message: `${obj.message.split(" ")[0]} has left`
                })
            }
        }
    })

    return answer.map(x=> x.message)
}




console.log(solution(["Enter uid1234 Muzi", "Enter uid4567 Prodo", "Leave uid1234", "Enter uid1234 Prodo", "Change uid4567 Ryan"]))
